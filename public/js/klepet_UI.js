/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://teaching.lavbic.net/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://teaching.lavbic.net/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://teaching.lavbic.net/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  var embed;
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/'||preveriOmembe(sporocilo)) {
    if(preveriOmembe(sporocilo)){
      sporocilo = filtrirajVulgarneBesede(sporocilo);
      klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      if(jeVideo(sporocilo)){
      var videos=dodajVideo(sporocilo);
      embed = obdelaj(videos);
      $('#sporocila').append(divElementHtmlTekst(embed));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      //klepetApp.posljiSporocilo(trenutniKanal, divElementHtmlTekst(embed));
    }  
    }
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    //ce je v sporocilu link od youtube pol se naredi tabela z html embed frami
    if(jeVideo(sporocilo)){
      var videos=dodajVideo(sporocilo);
      embed = obdelaj(videos);
      $('#sporocila').append(divElementHtmlTekst(embed));
      //klepetApp.posljiSporocilo(trenutniKanal, divElementHtmlTekst(embed));
    }  
  }
  $('#poslji-sporocilo').val('');
}

function preveriOmembe(sporocilo){
  for(var i in sporocilo){
    if(sporocilo.charAt(i) == '@')return true;
  }
  return false;
}

function dodajOmembe(sporocilo){
  var table = [];
  for(var i=0;i<sporocilo.length;i++){
    if(sporocilo.charAt(i) == '@'){
      table.push(sporocilo.substring(i+1,endWord(i+1,sporocilo)));
    }
  }
  return table;
}

function endWord(n,sporocilo){
  for(var i=n;i<sporocilo.length;i++){
    if(sporocilo.charAt(i)==' '){
      return i;
    }
  }
  return sporocilo.length;
}

function jeVideo(besedilo){
  var del = 'https://www.youtube.com/watch?v=';
  for(var i=0;i+del.length<=besedilo.length;i++){
    if(besedilo.substring(i,i+del.length)==del){
      return true;  
    }
  }
  return false;
}

function dodajVideo(sporocilo){
  var videi = [];
  var i = 1;
  var start = najdi(sporocilo,'https://www.youtube.com/watch?v=',i);
  while(start!=-1){
    start = najdi(sporocilo,'https://www.youtube.com/watch?v=',i);
    videi.push(sporocilo.substring(start+32,start+43));
    i++;
  }
  videi.splice(videi.length - 1,1);
  //console.log(videi);
  return videi;
}

function najdi(besedilo,del,instanca) {
	var count = 0;
	for(var i=0;i+del.length<=besedilo.length;i++){
    	if(besedilo.substring(i,i+del.length)==del){
        	count++;
            if(count==instanca){
            	return i;
            }
        }
    }
    return -1;
}

function obdelaj(tabela){
  var array = [];
  for(var i=0;i<tabela.length;i++){
    array.push('<iframe src="https://www.youtube.com/embed/'+tabela[i]+'" allowfullscreen style="height: 150px; width: 200px; padding-left: 10px; padding-top: 10px"></iframe>');
  }
  console.log(array);
  return array;
}
// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('./swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});


/**
 * Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj 
 * z enako dolžino zvezdic (*)
 * 
 * @param vhodni niz
 */
function filtrirajVulgarneBesede(vhod) {
  for (var i in vulgarneBesede) {
    var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
    vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
  }
  return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";


// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    var embed;
    if(jeVideo(sporocilo.besedilo)){
      var videos=dodajVideo(sporocilo.besedilo);
      embed = obdelaj(videos);
      $('#sporocila').append(divElementHtmlTekst(embed));
      //klepetApp.posljiSporocilo(trenutniKanal, divElementHtmlTekst(embed));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }  
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $("#seznam-uporabnikov").empty();
    for (var i=0; i < uporabniki.length; i++) {
      $("#seznam-uporabnikov").append(divElementEnostavniTekst(uporabniki[i]));
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});


/* global $, io, Klepet */